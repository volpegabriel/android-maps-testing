package ar.com.gmvsoft.mapstest.utils;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class Utils {

	public static JSONObject loadJSONFromAsset(String fileName, Context context) {
		try {
			InputStream is = context.getAssets().open(fileName);
			int size = is.available();

			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			String json = new String(buffer, "UTF-8");
			return new JSONObject(json);
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
