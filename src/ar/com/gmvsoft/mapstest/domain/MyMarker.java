package ar.com.gmvsoft.mapstest.domain;

import ar.com.gmvsoft.mapstest.dto.MyMarkerDTO;

import com.google.android.gms.maps.model.LatLng;

public class MyMarker {

	private LatLng latlon;
	private String title;
	private String snippet;
	private String keywords;
	private Integer stars;

	public MyMarker(LatLng latlon, String title, String snippet, String keywords, Integer stars) {
		this.latlon = latlon;
		this.title = title;
		this.snippet = snippet;
		this.keywords = keywords;
		this.stars = stars;
	}

	public MyMarker(MyMarkerDTO trade) {
		this(new LatLng(trade.getLatitude(), trade.getLongitude()), trade.getTitle(), trade.getSnippet(), trade.getKeywords(), 8);
	}

	public LatLng getLatlon() {
		return latlon;
	}

	public void setLatlon(LatLng latlon) {
		this.latlon = latlon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public boolean matchKeyword(String keyword) {
		String title = getTitle().toLowerCase();
		String snippet = getSnippet().toLowerCase();
		String keywords = getKeywords().toLowerCase();
		return (title.contains(keyword) || snippet.contains(keyword) || keywords.contains(keyword));
	}

}
