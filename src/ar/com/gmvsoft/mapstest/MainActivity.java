package ar.com.gmvsoft.mapstest;

import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import ar.com.gmvsoft.mapstest.domain.MyMarker;
import ar.com.gmvsoft.mapstest.service.MyMarkerService;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements OnInfoWindowClickListener, OnEditorActionListener {

	private GoogleMap map;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Search text
		EditText searchText = (EditText) findViewById(R.id.search_text);
		searchText.setOnEditorActionListener(this);
		
		// Map
		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.map);
		if (mapFragment != null) {
			map = mapFragment.getMap();

			if (map != null) {
				map.setOnInfoWindowClickListener(this);
				map.moveCamera(CameraUpdateFactory.newLatLngZoom(MyMarkerService.ETERMAX, 15));
			}
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		Intent intent = new Intent(this, PlaceActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString(PlaceActivity.TITLE, marker.getTitle());
		bundle.putString(PlaceActivity.SNIPPET, marker.getSnippet());
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		boolean handled = false;
		if (actionId == EditorInfo.IME_ACTION_DONE) {
			searchSubmit();
			handled = true;
		}
		return handled;
	}

	public void searchSubmit() {
		EditText searchText = (EditText) findViewById(R.id.search_text);
		String keyword = searchText.getText().toString();
		map.clear();
		findPlaceAndAddMarkers(keyword);
	}
	
	private void findPlaceAndAddMarkers(String keyword) {
		if (keyword != null && !keyword.isEmpty()) {
			hideKeyboard();
			
			Set<MyMarker> markers = MyMarkerService.getInstance(this).getMarkers();
			
			LatLng place = null;
			int placesResultSize = 0;
			
			for (MyMarker myMarker : markers) {
				if (myMarker.matchKeyword(keyword)) {
					placesResultSize++;
					place = myMarker.getLatlon();
	
					map.addMarker(new MarkerOptions()
					.position(myMarker.getLatlon())
					.title(myMarker.getTitle())
					.snippet(myMarker.getSnippet())
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_launcher)));
				}
			}
			
			if (placesResultSize == 1) {
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(place, 16);
				map.animateCamera(cameraUpdate);
			} else if (placesResultSize > 0) {
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(MyMarkerService.ETERMAX, 15);
				map.animateCamera(cameraUpdate);
			}
		}
	}

	private void showHideKeyboard(boolean show) {
		EditText searchText = (EditText) findViewById(R.id.search_text);
		InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		if (show)
			inputMethodManager.showSoftInput(searchText, 0);
		else
			inputMethodManager.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
	}
	
	private void hideKeyboard() {
		showHideKeyboard(false);
	}

	@SuppressWarnings("unused")
	private void addMarkers() {
		Set<MyMarker> markers = MyMarkerService.getInstance(this).getMarkers();
				
		for (MyMarker myMarker : markers) {
			map.addMarker(new MarkerOptions()
			.position(myMarker.getLatlon())
			.title(myMarker.getTitle())
			.snippet(myMarker.getSnippet())
			.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.ic_launcher)));
		}
	}
	
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }

}
