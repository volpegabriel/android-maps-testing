package ar.com.gmvsoft.mapstest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import ar.com.gmvsoft.mapstest.domain.MyMarker;
import ar.com.gmvsoft.mapstest.service.MyMarkerService;

public class PlaceActivity extends Activity {

	public static final String TITLE = "title";
	public static final String SNIPPET = "snippet";
	
	private View place;
	private TextView placeTitle;
	private TextView placeContent;
	private RatingBar placeRatingBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		String title = extras.getString(TITLE);
		String snippet = extras.getString(SNIPPET);
		
		MyMarker marker = MyMarkerService.getInstance(this).findByTitle(title);
		
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		place = layoutInflater.inflate(R.layout.place, null);

		placeTitle = (TextView) place.findViewById(R.id.placeTitle);
		placeTitle.setText(title);
		placeTitle.setTextColor(Color.GREEN);
		
		placeContent = (TextView) place.findViewById(R.id.placeContent);
		placeContent.setText(snippet);
		
		placeRatingBar = (RatingBar) place.findViewById(R.id.placeRatingBar);
		placeRatingBar.setProgress(marker.getStars());
		
		setContentView(place);
	}
	
}
