package ar.com.gmvsoft.mapstest.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import ar.com.gmvsoft.mapstest.domain.MyMarker;
import ar.com.gmvsoft.mapstest.dto.MyMarkerDTO;
import ar.com.gmvsoft.mapstest.utils.Utils;

import com.google.android.gms.maps.model.LatLng;

public class MyMarkerService {

	private static MyMarkerService instance;

	public static final LatLng ETERMAX = new LatLng(-34.56503, -58.494133);

	private Set<MyMarker> markers;
	private Context context;
	private JSONObject allJsonObjects;

	private static final List<String> tradeKeys = Arrays.asList("muerte", "familia", "bondio", "angelito", "chinos", "supasta", "morita", "krozz", "gyn");

	private MyMarkerService() {
	}

	public static MyMarkerService getInstance(Context context) {
		if (instance == null) {
			instance = new MyMarkerService();
			instance.setContext(context);
			instance.openTrades();
		}
		instance.setContext(context);
		return instance;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Set<MyMarker> getMarkers() {
		if (markers == null)
			buildMarkers();
		return markers;
	}

	public MyMarker findByTitle(String title) {
		for (MyMarker myMarker : markers) {
			if (myMarker.getTitle().equals(title))
				return myMarker;
		}
		throw new RuntimeException("MyMarker not found...");
	}

	private void buildMarkers() {
		markers = new HashSet<MyMarker>();
		markers.add(new MyMarker(ETERMAX, "Etermax", "Desarrollo de video juegos.", "empresa", 10));

		for (String trade : tradeKeys) {
			markers.add(new MyMarker(findTrade(trade)));
		}
	}

	private MyMarkerDTO findTrade(String key) {
		MyMarkerDTO myMarkerDTO = new MyMarkerDTO();
		try {
			JSONObject jsonObject = allJsonObjects.getJSONObject(key);
			myMarkerDTO.setTitle(jsonObject.getString("title"));
			myMarkerDTO.setSnippet(jsonObject.getString("snippet"));
			myMarkerDTO.setKeywords(jsonObject.getString("keywords"));
			myMarkerDTO.setLatitude(jsonObject.getDouble("latitude"));
			myMarkerDTO.setLongitude(jsonObject.getDouble("longitude"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return myMarkerDTO;
	}

	public void openTrades() {
		allJsonObjects = Utils.loadJSONFromAsset("trade.json", context);
	}

}
